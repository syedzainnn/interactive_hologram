﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizController : MonoBehaviour
{

	#region Variables Declaration

	// Question Booleans
	public bool Q1;
	private bool Q2;
	private bool Q3A,Q3B;
	private bool Q4A1, Q4A2, Q4B1, Q4B2;
	private bool Quit_;
	private bool Ending;

	// Button Booleans
	private bool Button_NO, Button_YES;
	private bool Button_Correct, Button_Wrong;
	private bool Button_History, Button_Science;
	private bool Button_Agree, Button_Disagree;
	private bool Button1_4A1, Button2_4A1;
	private bool Button1_4A2, Button2_4A2;
	private bool Button1_4B1, Button2_4B1;
	public bool Button1_4B2, Button2_4B2;

	private bool StartTimer = false;
	

	private float timeElapsed;


	public GameObject Question1;
	public GameObject QuitWindow;
	public GameObject Question2;
	public GameObject Question3A;
	public GameObject Question3B;
	public GameObject Question4A1, Question4A2;
	public GameObject Question4B1, Question4B2;
	public GameObject FinalStatement;

	#endregion

	void Start ()
	{
		Q1 = Q2 = Q3A = Q3B = Q4A1 = Q4A2 = Q4B1 = Q4B2 = Quit_ = false;

		Button_NO = Button_YES =  Button_Correct = Button_Wrong = Button_History = Button_Science = Button_Agree 
		= Button_Disagree = Button1_4A1 = Button2_4A1 = Button1_4A2 = Button2_4A2 = Button1_4B1 = Button2_4B1 = 
		Button1_4B2 = Button2_4B2 = false;

		timeElapsed = 0f;
		Question1.SetActive(false);
		QuitWindow.SetActive(false);
		Question2.SetActive(false);
		Question3A.SetActive(false);
		Question3B.SetActive(false);
		Question4A1.SetActive(false);
		Question4A2.SetActive(false);
		Question4B1.SetActive(false);
		Question4B2.SetActive(false);
		FinalStatement.SetActive(false);
		
		StartTimer = false;
	}

	private void Update()
	{
		if (StartTimer == true )
		{

			//Handling Question 1
			if (Q1 == true && timeElapsed <= 3f) 
			{
				if (Button_NO == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question1.SetActive(false);
						QuitWindow.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_NO = false;
					}
				}
				else if (Button_YES == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question1.SetActive(false);
						Question2.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_YES = false;
					}
				}
			}


			//Handing Question 2
			if (Q2 == true && timeElapsed <= 3f)
			{
				if (Button_Correct == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question2.SetActive(false);
						Question3A.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_Correct = false;
					}
				}

				else if (Button_Wrong == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question2.SetActive(false);
						Question3B.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_Wrong = false;
					}
				}
			}

			//Handling Question 3A
			if (Q3A == true && timeElapsed <= 3f)
			{
				if (Button_History == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question3A.SetActive(false);
						Question4A1.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button_History = false;
					}
				}

				else if (Button_Science == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question3A.SetActive(false);
						Question4A2.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_Science = false;
					}
				}
			}


			//Handling Question 3B
			if (Q3B == true && timeElapsed <= 3f)
			{
				if (Button_Agree == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question3B.SetActive(false);
						Question4B1.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button_History = false;
					}
				}

				else if (Button_Disagree == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question3B.SetActive(false);
						Question4B2.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button_Science = false;
					}
				}
			}


			//Handling Question 4A1
			if (Q4A1 == true && timeElapsed <= 3f)
			{
				if (Button1_4A1 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4A1.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button1_4A1 = false;
					}
				}

				else if (Button2_4A1 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4A1.SetActive(false);
						FinalStatement.SetActive(true);
						StartTimer = false;
						timeElapsed = 0f;
						Button2_4A1 = false;
					}
				}
			}


			//Handling Question 4A2
			if (Q4A2 == true && timeElapsed <= 3f)
			{
				if (Button1_4A2 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4A2.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button1_4A2 = false;
					}
				}

				else if (Button2_4A2 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4A2.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button2_4A2 = false;
					}
				}
			}


			//Handling Question 4B1
			if (Q4B1 == true && timeElapsed <= 3f)
			{
				if (Button1_4B1 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4B1.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button1_4B1 = false;
					}
				}

				else if (Button2_4B1 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4B1.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button2_4B1 = false;
					}
				}
			}



			//Handling Question 4B2
			if (Q4B2 == true && timeElapsed <= 3f)
			{
				if (Button1_4B2 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4B2.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button1_4B2 = false;
					}
				}

				else if (Button2_4B2 == true)
				{
					timeElapsed += Time.deltaTime;
					if (timeElapsed >= 3f)
					{
						Question4B2.SetActive(false);
						FinalStatement.SetActive(true);

						StartTimer = false;
						timeElapsed = 0f;
						Button2_4B2 = false;
					}
				}
			}





		}


		#region Button Functions

	}
	public void QuestionHandler()
	{
		Q1 = true;
		Question1.SetActive(true);
	}

	public void OnNo()
	{
		Button_NO = true;
		//Button_YES = false;
		StartTimer = true;
		Q2 = false;
	}


	public void OnYes()
	{
		Button_YES = true;
		//Button_NO = false;
		StartTimer = true;
		//Q2 = true;
	}


	public void OnCorrect()
	{
		Button_Correct = true;
		//Button_Wrong = false;
		StartTimer = true;
		Q2 = true;
	}


	public void OnWrong()
	{
		Button_Wrong = true;
		//Button_Correct = false;
		StartTimer = true;
		Q2 = true;
	}

	public void OnHistory()
	{
		Button_History = true;
		//Button_Science = false;
		StartTimer = true;
		Q3A = true;
	}

	public void OnScience()
	{
		Button_Science = true;
		//Button_History = false;
		StartTimer = true;
		Q3A = true;
	}
	public void OnAgree()
	{
		Button_Agree = true;
		//Button_Disagree = false;
		StartTimer = true;
		Q3B = true;
	}

	public void OnDisagree()
	{
		Button_Disagree = true;
		//Button_Agree = false;
		StartTimer = true;
		Q3B = true;
	}

	public void OnB1_4A1()
	{
		Button1_4A1 = true;
		//Button2_4A1 = false;
		StartTimer = true;
		Q4A1 = true ;
	}

	public void OnB2_4A1()
	{
		Button2_4A1 = true;
		//Button1_4A1 = false;
		StartTimer = true;
		Q4A1 = true;
	}

	public void OnB1_4A2()
	{
		Button1_4A2 = true;
		//Button2_4A2 = false;
		StartTimer = true;
		Q4A2 = true;
	}

	public void OnB2_4A2()
	{
		Button2_4A2 = true;
		//Button1_4A2 = false;
		StartTimer = true;
		Q4A2 = true;
	}

	public void OnB1_4B1()
	{
		Button1_4B1 = true;
		//Button2_4B1 = false;
		StartTimer = true;
		Q4B1 = true;

	}
	public void OnB2_4B1()
	{
		Button2_4B1 = true;
		//Button1_4B1 = false;
		StartTimer = true;
		Q4B1 = true;
	}

	public void OnB1_4B2()
	{
		Button1_4B2 = true;
		//Button2_4B2 = false;
		StartTimer = true;
		Q4B2 = true;
	}


	public void OnB2_4B2()
	{
		Button2_4B2 = true;
		//Button1_4B2 = false;
		StartTimer = true;
		Q4B2 = true;
	}


	#endregion
}
