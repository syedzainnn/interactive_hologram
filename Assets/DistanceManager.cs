﻿using UnityEngine;
using System.Collections;

public class DistanceManager : MonoBehaviour
{
	public GameObject UIInstruction;
	private bool Q1;
	private bool Q2;
	private bool Q3;

	public GameObject Question1;
	public GameObject Question2;

	public GameObject QuitWindow;

	private QuizController QC;

	private bool FirstTime ;

	private KinectManager kinectManager;
	int playerIndex = 0;
	private Vector3 UserPosition = new Vector3(0,0,0);


	void Start()
	{
		QC = GameObject.FindObjectOfType<QuizController>();
		FirstTime = true;
		UIInstruction.SetActive(false);
		kinectManager = KinectManager.Instance;
	}

	void Update()
	{
		if (kinectManager && kinectManager.IsInitialized())
		{

			long userId = kinectManager.GetUserIdByIndex(playerIndex);

			if (kinectManager.IsUserTracked(userId))
			{
				UserPosition = kinectManager.GetUserPosition(userId);

				if (Mathf.Abs(UserPosition.z) > 1.30f)
				{
					//UIInstruction.SetActive(true);

				}
				else if (Mathf.Abs(UserPosition.z) <= 1.25f)

				{
					if ((QC.Q1 == false) && FirstTime == true )
					{
						QC.QuestionHandler();
						FirstTime = false;
					}

				}

				else
				{
					UIInstruction.SetActive(false);

					
				}



			}
			
		}
	}
	
}
