﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeechController : MonoBehaviour, SpeechRecognitionInterface
{
	private SpeechManager speechManager;
	public Button YES, NO, CORRECT, WRONG;
	private Animator anim;

	public bool SpeechPhraseRecognized(string phraseTag, float condidence)
	{
		switch (phraseTag)
		{
			case "YES":
				anim = YES.GetComponent<Animator>();
				anim.SetTrigger("Pressed");
				YES.onClick.Invoke();
				break;
			case "NO":
				anim = NO.GetComponent<Animator>();
				anim.SetTrigger("Pressed");
				NO.onClick.Invoke();
				break;
			case "CORRECT":
				anim = CORRECT.GetComponent<Animator>();
				anim.SetTrigger("Pressed");
				CORRECT.onClick.Invoke();
				Debug.Log("R");
				break;
			case "WRONG":
				anim = WRONG.GetComponent<Animator>();
				anim.SetTrigger("Pressed");
				WRONG.onClick.Invoke();
				Debug.Log("F");

				break;
		}

		return true;
	}

}
